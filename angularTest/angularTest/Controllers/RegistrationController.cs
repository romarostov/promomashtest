﻿using angularTest.dal.dto;
using angularTest.dal.services;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace angularTest.Controllers
{
    [Route("api/registration")]
    [ApiController]
    public class RegistrationController:Controller
    {
        private readonly IRegistrationService registrationService;

        public RegistrationController(IRegistrationService registrationService)
        {
            this.registrationService = registrationService;
        }

        [HttpPost]
        public Task<RegistrationRespone> Register(RegistrationRequest request)
        => registrationService.Register(request);

        [HttpGet("isEmailAllreadyUsed/{email}")]
        public Task<bool> IsEmailAllreadyUsed(string email)
        => registrationService.IsEmailAllreadyUsed(email);

        [HttpGet("list")]
        public Task<RegistrationResponseItem[]> GetAllRegistration()
        => registrationService.GetAllRegistration();

    }
}
