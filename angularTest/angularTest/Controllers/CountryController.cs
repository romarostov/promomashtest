﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using angularTest.dal.dto;
using angularTest.dal.services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace angularTest.Controllers
{
    [Route("api/conuntry")]
    [ApiController]
    public class CountryController : Controller
    {
        private readonly ICountryService countryService;
        private readonly IProvinceService provinceService;

        public CountryController(ICountryService countryService, IProvinceService provinceService)
        {
            this.countryService = countryService;
            this.provinceService = provinceService;
        }

        [HttpGet("all")]
        public async Task<IEnumerable<CountryResponse>> GetCountries()
        {
            var countries =await countryService.GetCountries();
            return countries;            
        }

        [HttpGet("{countryId}/provinces")]
        public async Task<IEnumerable<ProvinceResponse>> GetCountries(Guid countryId)
        {
            var countries = await provinceService.GetProvincesByCountryId(countryId);
            return countries;
        }

    }
}