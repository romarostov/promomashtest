import { Component, OnInit } from '@angular/core';
import { RegistrationResponseItem } from "./registrationResponseItem";
import { RegistrationUsersService } from "./registration-users.service";
import { MatSnackBar } from "@angular/material/snack-bar";

@Component({
  selector: 'app-registered-users',
  templateUrl: './registered-users.component.html',
  styleUrls: ['./registered-users.component.less']
})
export class RegisteredUsersComponent implements OnInit {

  constructor(private userSerivce: RegistrationUsersService,
    private snackBar: MatSnackBar) { }

  users: RegistrationResponseItem[]

  ngOnInit() {
    this.userSerivce.getAllRegistration()
      .subscribe((users) => {
        this.users = users;
      },
      (error) => {
        console.log(error);
        this.snackBar.open("Failed load", '', {
          duration: 2000
        });
      })
  };

}
