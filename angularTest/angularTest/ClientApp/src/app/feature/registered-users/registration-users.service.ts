import { Inject, Injectable, Optional } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Observable } from 'rxjs';

import { RegistrationResponseItem } from './registrationResponseItem';

@Injectable()
export class RegistrationUsersService {

    constructor(private httpClient: HttpClient,
        @Inject('BASE_URL') private basePath: string) {
    }

    public getAllRegistration(): Observable<RegistrationResponseItem[]> {
        return this.httpClient.get<RegistrationResponseItem[]>(`${this.basePath}/api/registration/list`);
    }
}
