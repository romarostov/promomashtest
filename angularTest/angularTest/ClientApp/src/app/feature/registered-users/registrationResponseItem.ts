export interface RegistrationResponseItem { 
    email?: string;
    country?: string;
    province?: string;
    registrationDateTime?: Date;
}
