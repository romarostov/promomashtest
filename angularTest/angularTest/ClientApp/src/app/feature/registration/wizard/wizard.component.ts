import { Component, ViewChild } from '@angular/core';

import { RegistrationService } from "../services/registration.service";

import { Step1Component } from "../step1/step1.component";
import { Step2Component } from "../step2/step2.component";
import { StepperSelectionEvent } from "@angular/cdk/stepper";
import { RegistrationRequest } from "src/app/feature/registration/models/registrationRequest";
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from "@angular/router";

@Component({
  selector: 'app-wizard',
  templateUrl: './wizard.component.html',
  styleUrls: ['./wizard.component.less']
})
export class WizardComponent {

  constructor(private registrationService: RegistrationService,
    private snackBar: MatSnackBar,
    private router: Router) { }

  @ViewChild('step1', { static: true }) step1: Step1Component;
  @ViewChild('step2', { static: true }) step2: Step2Component;

  onSelectionTabChanged(event: StepperSelectionEvent) {
    console.log(event);
    if (event && event.selectedIndex == 1) {
      this.step2.onOpenForm();
    }
  }

  savingInProgress: boolean = false;

  onSubmit() {
    let data: RegistrationRequest = {};
    this.step1.fillRequestData(data);
    this.step2.fillRequestData(data);

    this.savingInProgress = true;
    this.registrationService.register(data)
      .subscribe((response) => {
        this.savingInProgress = false;
        this.router.navigateByUrl('/users');
      }, error => {
        this.savingInProgress = false;
        console.log(error);
        this.snackBar.open("Failed save", '', {
          duration: 2000,
          panelClass: ['snackBarGreen']
        });
      });
  }
}
