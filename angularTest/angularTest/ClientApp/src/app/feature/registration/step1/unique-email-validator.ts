import { AsyncValidator, ValidationErrors, AbstractControl, AsyncValidatorFn } from "@angular/forms/forms";
import { Promise } from "q";
import { RegistrationService } from "../services/registration.service";
import { map, switchMap, catchError } from "rxjs/operators";
import { Observable } from "rxjs";
import { of, timer } from 'rxjs';

export function uniqueEmailValidator(registrationService: RegistrationService): AsyncValidatorFn {
  return (control: AbstractControl) => {
    return timer(500).pipe(
      switchMap(() => {
        if (!control.value) {
          return of(null);
        }
        return registrationService.isEmailAllreadyUsed(control.value)
          .pipe(
            map(isUsed => isUsed ? { 'uniqueEmail': true } : null),
            catchError(error => {
              console.error(error);
              return of({ 'uniqueEmailFailed': true });
            })          
          )
      })
    );
  }
};


