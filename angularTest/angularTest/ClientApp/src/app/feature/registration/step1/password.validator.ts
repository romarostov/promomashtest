
import { AbstractControl, ValidationErrors } from "@angular/forms/forms";

export function passwordValidator(control: AbstractControl):
    ValidationErrors | null {
    const password = control.value;
    if (password && password.length > 0) {

        let errorMessage: string = null;

        if (password.length < 3) {
            errorMessage="Password must contains min 3 symbols";
        }
        else {
            let existsSpace = /.*\s.*/.test(password);

            if (existsSpace) {
                errorMessage="Password cannot contains spaces";
            } else {

                let notExistsLetter = !/.*[a-zA-Z].*/.test(password);
                let notExistsDigit = !/.*[0-9].*/.test(password);

                if (notExistsLetter && notExistsDigit) {
                    errorMessage = "Password must contains min 1 digit and 1 letter";
                }
                else if (notExistsDigit){
                    errorMessage = "Password must contains min 1 digit";
                }
                else if (notExistsLetter) {
                    errorMessage = "Password must contains min 1 letter";
                }
            }
        }
        if (errorMessage) {
            return {
                "errorPassword": {
                    value: errorMessage
                }
            };
        }

    }

    return null;
};
