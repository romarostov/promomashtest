import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, AbstractControl, ValidationErrors } from "@angular/forms";
import { uniqueEmailValidator } from "./unique-email-validator";
import { RegistrationService } from "../services/registration.service";
import { passwordValidator } from "./password.validator";
import { FormErrorStateMatcher } from "../FormErrorStateMatcher";
import { RegistrationRequest } from "src/app/feature/registration/models/registrationRequest";

const defaultRequiredMessage = 'You must enter a value';

@Component({
  selector: 'registration-step1',
  templateUrl: './step1.component.html',
  styleUrls: ['./step1.component.less']
})
export class Step1Component {

  constructor(private fb: FormBuilder,
    private registrationService: RegistrationService, ) { }

  firstFormGroup: FormGroup = this.fb.group(
    {
      login: ['',
        [Validators.required, Validators.email],
        [uniqueEmailValidator(this.registrationService)]
      ],
      password: ['',
        [Validators.required, passwordValidator]
      ],
      confirmPassword: ['', [Validators.required]],
      agree: [false, Validators.requiredTrue]
    },
    {
      validators: Step1Component.passwordsValidatorValidator
    }
  );


  fillRequestData(data: RegistrationRequest) {
    data.email = this.login.value;
    data.password = this.password.value;
  }

  confirmErrorState = new FormErrorStateMatcher('passwordsMisMatch');

  static passwordsValidatorValidator(control: AbstractControl): ValidationErrors | null {
    const password = control.get("password");
    if (password.valid) {
      const confirmPassword = control.get("confirmPassword");
      if (password.value !== confirmPassword.value) {
        return { "passwordsMisMatch": true };
      }
    }
    return null;
  };

  onNextClick() {
    this.agree.markAsTouched();
  }

  get agree() {
    return this.firstFormGroup.get("agree");
  }

  get login() {
    return this.firstFormGroup.get("login");
  }

  getEmailErrorMessage() {
    return this.login.hasError('required') ? defaultRequiredMessage :
      this.login.hasError('email') ? 'Login must be a valid email' :
        this.login.hasError('uniqueEmail') ? 'Email already used.' :
          this.login.hasError('uniqueEmailFailed') ? 'Failed check email.' :
            '';
  }

  get password() {
    return this.firstFormGroup.get("password")
  }

  getPasswordErrorMessage() {
    return this.password.hasError('required') ? defaultRequiredMessage :
      this.password.hasError('errorPassword') ? this.password.errors.errorPassword.value :
        '';
  }

  get confirmPassword() {
    return this.firstFormGroup.get("confirmPassword");
  }

  getConfirmPasswordErrorMessage() {
    return this.confirmPassword.hasError('required') ? defaultRequiredMessage
      : this.firstFormGroup.hasError("passwordsMisMatch")
        ? 'Confirm password must be the same with password' : 'sdfsd';
  }

}
