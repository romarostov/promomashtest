
import {FormControl, FormGroupDirective, NgForm, Validators} from '@angular/forms';
import {ErrorStateMatcher} from '@angular/material/core';



export class FormErrorStateMatcher implements ErrorStateMatcher {

    constructor(private formErrorName:string) {       
        
    }

    isErrorState(control: FormControl | null, form: FormGroupDirective | NgForm | null): boolean {
      const isSubmitted = form && form.submitted;
      const formInValid= form && form.hasError(this.formErrorName);
      return !!(control 
        && (control.invalid || formInValid) 
        && (control.dirty || control.touched || isSubmitted));
    }
  }