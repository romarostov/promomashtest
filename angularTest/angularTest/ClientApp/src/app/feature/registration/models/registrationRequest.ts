export interface RegistrationRequest { 
    email?: string;
    password?: string;
    countryId?: string;
    provinceId?: string;
}
