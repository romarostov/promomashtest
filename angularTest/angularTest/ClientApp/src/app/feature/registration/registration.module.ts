import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { WizardComponent } from './wizard/wizard.component';
import { MaterialModule } from "src/app/material/material.module";
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import {HttpClientModule} from '@angular/common/http';
import { RegistrationService } from "./services/registration.service";
import { CountryService } from "./services/country.service";
import { Step1Component } from './step1/step1.component';
import { Step2Component } from './step2/step2.component';

@NgModule({
  imports: [
    CommonModule,   
    MaterialModule, 
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    RouterModule.forChild([
      { path: '', component: WizardComponent }      
    ])
  ],
  declarations: [WizardComponent, Step1Component, Step2Component],  
  providers: [
    RegistrationService,
    CountryService ]
})
export class RegistrationModule { }
