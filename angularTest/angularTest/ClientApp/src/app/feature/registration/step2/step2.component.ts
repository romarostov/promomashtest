import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { CountryService } from "../services/country.service";

import { CountryResponse } from "../models/countryResponse";
import { ProvinceResponse } from "../models/provinceResponse";
import { RegistrationRequest } from "src/app/feature/registration/models/registrationRequest";

@Component({
  selector: 'registration-step2',
  templateUrl: './step2.component.html',
  styleUrls: ['./step2.component.less']
})
export class Step2Component implements OnInit {

  constructor(private fb: FormBuilder,
    private countryService: CountryService) { }

  secondFormGroup: FormGroup = this.fb.group({
    country: ['', Validators.required],
    province: ['', Validators.required],
  });

  countries: CountryResponse[];
  provinces: ProvinceResponse[];
  lastLoadedProvincesCountryId: string;

  @Output("onSaveClick") saveEvent = new EventEmitter();

  ngOnInit() {
    this.secondFormGroup.get('country').valueChanges.subscribe(countryId => {
      if (!countryId) {
        this.province.reset();
      }
      else if (this.lastLoadedProvincesCountryId != countryId) {
        this.countryService.getCountriyProvinces(countryId)
          .subscribe(provinces => {
            this.lastLoadedProvincesCountryId = countryId;
            this.provinces = provinces
            this.province.setErrors({ "failedLoadProvinces": null });
          },
          error => {
            console.log(error);
            this.province.setErrors({ "failedLoadProvinces": true });
            this.province.markAsTouched();
          });
      }
    });
  }

  fillRequestData(data: RegistrationRequest) {
    data.countryId=this.country.value;
    data.provinceId=this.province.value;
  };

  onOpenForm() {
    if (this.countries == null) {
      this.countryService.getCountries()
        .subscribe(countries => {
          this.countries = countries;
          this.country.setErrors({ "failedLoadCountries": null });
        },
        error => {
          console.log(error);
          this.country.setErrors({ "failedLoadCountries": true });
          this.country.markAsTouched();
        });
    }
  }




  get country() {
    return this.secondFormGroup.get("country");
  }

  get province() {
    return this.secondFormGroup.get("province");
  }

  onSave() {
    if (this.secondFormGroup.valid) {
      this.saveEvent.emit();
    }
    else console.log("not valid");
  }

}
