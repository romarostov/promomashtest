import { Inject, Injectable, Optional } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Observable } from 'rxjs';

import { CountryResponse } from "../models/countryResponse";
import { ProvinceResponse } from '../models/provinceResponse';

@Injectable()
export class CountryService {

    constructor(private httpClient: HttpClient,
        @Inject('BASE_URL') private basePath: string) {
    }

    public getCountries(): Observable<CountryResponse[]> {
        return this.httpClient.get<CountryResponse[]>(`${this.basePath}api/conuntry/all`);
    }

    public getCountriyProvinces(countryId: string): Observable<ProvinceResponse[]> {

        if (countryId === null || countryId === undefined) {
            throw new Error('Required parameter countryId was null or undefined when calling getCountries_1.');
        }
        return this.httpClient.get<ProvinceResponse[]>(`${this.basePath}api/conuntry/${countryId}/provinces`);
    }

}
