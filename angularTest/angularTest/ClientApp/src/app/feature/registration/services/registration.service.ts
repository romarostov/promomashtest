import { Inject, Injectable, Optional } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Observable } from 'rxjs';

import { RegistrationRequest } from '../models/registrationRequest';
import { RegistrationRespone } from '../models/registrationRespone';

@Injectable()
export class RegistrationService {

    constructor(private httpClient: HttpClient,
        @Inject('BASE_URL') private basePath: string) {
    }

    public isEmailAllreadyUsed(email: string): Observable<boolean> {
        if (email === null || email === undefined) {
            throw new Error('Required parameter email was null or undefined when calling isEmailAllreadyUsed.');
        }
        return this.httpClient.get<boolean>(`${this.basePath}api/registration/isEmailAllreadyUsed/${email}`);
    }

    public register(request: RegistrationRequest): Observable<RegistrationRespone> {
        return this.httpClient.post<RegistrationRespone>(`${this.basePath}api/registration`,
            request);
    }

}
