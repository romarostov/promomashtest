import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { RegisteredUsersComponent } from "src/app/feature/registered-users/registered-users.component";


const routes: Routes = [
  { path: 'register', loadChildren: "./feature/registration/registration.module#RegistrationModule" },
  { path: 'users', component: RegisteredUsersComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
