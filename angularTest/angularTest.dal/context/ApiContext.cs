﻿using angularTest.dal.entities;
using Microsoft.EntityFrameworkCore;

namespace angularTest.dal.context
{
    public class ApiContext : DbContext
    {
        public ApiContext(DbContextOptions<ApiContext> options)
            : base(options)
        {
        }

        public DbSet<RegistrationEntity> Registrations { get; set; }

        public DbSet<CountryEntity> Countries { get; set; }

        public DbSet<ProvinceEntity> Provinces { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<CountryEntity>()
                .HasMany(c => c.Provinces)
                .WithOne(p => p.Country).IsRequired();

            modelBuilder.Entity<RegistrationEntity>()
            .HasOne(r => r.Country).WithMany(x => x.Registrations).IsRequired();

            modelBuilder.Entity<RegistrationEntity>()
                .HasOne(r => r.Province).WithMany(x => x.Registrations).IsRequired();

            base.OnModelCreating(modelBuilder);
        }
    }
}
