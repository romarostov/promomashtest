﻿using angularTest.dal.context.services;
using angularTest.dal.services;
using Microsoft.Extensions.DependencyInjection;

namespace angularTest.dal.context
{
    public static class ApiContextServiceRegistrator
    {
        public static void RegisterApiContextServices(this IServiceCollection services)
        {
            services.AddScoped<ICountryService, CountryService>();
            services.AddScoped<IProvinceService, ProvinceService>();
            services.AddScoped<IRegistrationService, RegistrationService>();
        }
    }
}
