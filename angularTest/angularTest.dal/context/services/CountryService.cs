﻿using angularTest.dal.dto;
using angularTest.dal.services;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading.Tasks;

namespace angularTest.dal.context.services
{
    internal class CountryService : ICountryService
    {
        private readonly ApiContext context;

        public CountryService(ApiContext context)
        {
            this.context = context;
        }

        public Task<CountryResponse[]> GetCountries()
        => context.Countries.OrderBy(c=>c.Name).Select(c => new CountryResponse
            {
                Id = c.Id,
                Name = c.Name
            }).ToArrayAsync();                    
    }
}
