﻿using angularTest.dal.dto;
using angularTest.dal.services;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace angularTest.dal.context.services
{
    internal class ProvinceService : IProvinceService
    {
        private readonly ApiContext context;

        public ProvinceService(ApiContext context)
        {
            this.context = context;
        }
        
        public Task<ProvinceResponse[]> GetProvincesByCountryId(Guid countryId)
        => context.Provinces.Where(p=>p.Country.Id==countryId).OrderBy(p=>p.Name).Select(c => new ProvinceResponse
        {
            Id = c.Id,
            Name = c.Name
        }).ToArrayAsync();
    }
}
