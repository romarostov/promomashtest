﻿using angularTest.dal.dto;
using angularTest.dal.entities;
using angularTest.dal.services;
using Microsoft.EntityFrameworkCore;
using System;
using System.ComponentModel.DataAnnotations;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace angularTest.dal.context.services
{
    internal class RegistrationService : IRegistrationService
    {
        private readonly ApiContext apiContext;

        public RegistrationService(ApiContext apiContext)
        {
            this.apiContext = apiContext;
        }

        public async Task<bool> IsEmailAllreadyUsed(string email)
        {
            if (email.Contains("error")) throw new InvalidDataException("test exception");

            ValidateEmail(email);            
            var lowEmail = ConvertEmailToLowcase(email);
            var used =  await apiContext.Registrations
                .Where(x => x.EmailLowCase == lowEmail)
                .AnyAsync();
            return used;
        }

        public async Task<RegistrationRespone> Register(RegistrationRequest request)
        {
            if (request == null) throw new ArgumentNullException(nameof(request));

            if (await IsEmailAllreadyUsed(request.Email))
                throw new InvalidDataException("email already in use");

            CheckPassword(request.Password);
            
            var locationCorrect = await apiContext.Provinces
                .Where(p => p.Id == request.ProvinceId && p.Country.Id == request.CountryId)
                .AnyAsync();

            if (!locationCorrect) 
                throw new InvalidDataException($"Unknow province[{request.ProvinceId}] or country[{request.CountryId}]");

            var newRegistration = new RegistrationEntity(){
                Email=request.Email,
                EmailLowCase=ConvertEmailToLowcase(request.Email),
                Password=request.Password,
                ProvinceId =request.ProvinceId,
                CountryId = request.CountryId,
                RegistrationDateTime=DateTime.UtcNow
            };
            apiContext.Registrations.Add(newRegistration);

            await apiContext.SaveChangesAsync();

            return new RegistrationRespone()
            {
                Id = newRegistration.Id
            };
        }

        public async Task<RegistrationResponseItem[]> GetAllRegistration()
        {
            var registrations = await apiContext.Registrations
                .Include(x => x.Country).Include(x => x.Province)
                .Select(r => new RegistrationResponseItem
                {
                    Email = r.Email,
                    RegistrationDateTime = r.RegistrationDateTime,
                    Province = r.Province.Name,
                    Country = r.Country.Name
                }).ToArrayAsync();
            return registrations;
        }

        private static void CheckPassword(string password)
        {
            if (string.IsNullOrWhiteSpace(password))
                throw new InvalidDataException("Empty password");

            if (password.Length < 2) throw new InvalidDataException("Short password");

            if (Regex.IsMatch(password, @"\s"))
                throw new InvalidDataException("The password must not contain spaces");

            if (!Regex.IsMatch(password, @"\d"))
                throw new InvalidDataException("Password must contain min 1 digit");

            if (!Regex.IsMatch(password, @"[a-zA-Z]"))
                throw new InvalidDataException("Password must contain min 1 letter");
                        
        }

        string ConvertEmailToLowcase(string email) => email.Trim().ToLower();

        void ValidateEmail(string email)
        {
            if (string.IsNullOrWhiteSpace(email)) throw new InvalidDataException("Empty email");
            if(!new EmailAddressAttribute().IsValid(email))
            {
                throw new InvalidDataException("Email not valid");
            }
        }

    }
}
