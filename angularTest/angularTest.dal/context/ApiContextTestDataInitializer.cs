﻿using System;
using System.Collections.Generic;
using System.Linq;
using angularTest.dal.entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;

namespace angularTest.dal.context
{
    public static class ApiContextTestDataInitializer
    {

        public static void UseImMemoryApiContext(this IServiceCollection services)
        {
            services.AddDbContext<ApiContext>(opt => opt.UseInMemoryDatabase("api_database_name"));
        }

        public static void InitApiContextTestData(this IServiceProvider serviceProvider)
        {
            using (var scope = serviceProvider.CreateScope())
            {
                using (var context = scope.ServiceProvider.GetService<ApiContext>())
                {
                    context.Countries.Add(new CountryEntity
                    {
                        Name = "USA",
                        Provinces = InitProvincies("Oregon", "Washington", "Alaska")
                    });

                    var russia = new CountryEntity
                    {
                        Name = "Russia",
                        Provinces = InitProvincies("Rostov Oblast", "Moscow", "Crimea")
                    };
                    context.Countries.Add(russia);

                    context.Registrations.Add(new RegistrationEntity
                    {
                        Email = "romarostov@gmail.com",
                        EmailLowCase = "romarostov@gmail.com",
                        Country = russia,
                        Province = russia.Provinces.First(),
                        Password = "123456",
                        RegistrationDateTime = DateTime.UtcNow
                    });

                    context.SaveChanges();
                }
            }


            ICollection<ProvinceEntity> InitProvincies(params string[] names)
            => names.Select(n => new ProvinceEntity { Name = n }).ToArray();

        }
    }
}
