﻿using System;

namespace angularTest.dal.dto
{
    public class ProvinceResponse
    {
        public Guid Id { get; set; }

        public string Name { get; set; }
    }
}
