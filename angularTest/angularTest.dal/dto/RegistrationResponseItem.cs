﻿using System;

namespace angularTest.dal.dto
{
    public class RegistrationResponseItem
    {
        public string Email { get; set; }

        public string Country { get; set; }

        public string Province { get; set; }

        public DateTime RegistrationDateTime { get; set; }
    }

}
