﻿using System;

namespace angularTest.dal.dto
{

    public class CountryResponse
    {
        public Guid Id { get; set; }

        public string Name { get; set; }
    }
}
