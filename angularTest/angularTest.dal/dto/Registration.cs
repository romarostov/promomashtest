﻿using System;
using System.Collections.Generic;
using System.Text;

namespace angularTest.dal.dto
{
    public class RegistrationRequest
    {
        public string Email { get; set; }
        
        public string Password { get; set; }
        
        public Guid CountryId { get; set; }

        public Guid ProvinceId { get; set; }
    }

    public class RegistrationRespone
    {
        public Guid Id { get; set; }
    }

}
