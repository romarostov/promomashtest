﻿using angularTest.dal.dto;
using System.Threading.Tasks;

namespace angularTest.dal.services
{
    public interface ICountryService
    {
        Task<CountryResponse[]> GetCountries();
    }
}
