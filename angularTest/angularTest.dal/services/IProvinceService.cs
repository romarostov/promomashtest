﻿using angularTest.dal.dto;
using System;
using System.Threading.Tasks;

namespace angularTest.dal.services
{
    public interface IProvinceService
    {
        Task<ProvinceResponse[]> GetProvincesByCountryId(Guid countryId);
    }
}
