﻿using angularTest.dal.dto;
using System;
using System.Threading.Tasks;

namespace angularTest.dal.services
{
    public interface IRegistrationService
    {
        Task<bool> IsEmailAllreadyUsed(string email);

        Task<RegistrationRespone> Register(RegistrationRequest request);

        Task<RegistrationResponseItem[]> GetAllRegistration();
    }

}
