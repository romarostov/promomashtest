﻿using System;
using System.Collections.Generic;

namespace angularTest.dal.entities
{
    public class ProvinceEntity
    {
        public Guid Id { get; set; }

        public string Name { get; set; }

        public CountryEntity Country { get; set; }

        public ICollection<RegistrationEntity> Registrations { get; set; }
    }
}
