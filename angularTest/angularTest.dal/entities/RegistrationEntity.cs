﻿using System;

namespace angularTest.dal.entities
{

    public class RegistrationEntity
    {
        public Guid Id { get; set; }

        public string Email { get; set; }

        public string EmailLowCase { get; set; }

        public string Password { get; set; }

        public DateTime RegistrationDateTime { get; set; }
                
        public Guid CountryId { get; set; }

        public virtual CountryEntity Country { get; set; }

        public Guid ProvinceId { get; set; }

        public virtual ProvinceEntity Province { get; set; }        

    }
}
