﻿using System;
using System.Collections.Generic;

namespace angularTest.dal.entities
{
    public class CountryEntity
    {
        public Guid Id { get; set; }

        public string Name { get; set; }

        public ICollection<RegistrationEntity> Registrations { get; set; }        

        public ICollection<ProvinceEntity> Provinces { get; set; }

    }
}
